import { BrowserRouter, Routes, Route, } from "react-router-dom";


import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './conferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from "./PresentationForm";
import MainPage from "./Mainpage";

import Nav from './Nav'

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="locations/new" element={<LocationForm />} />
          <Route path="conferences/new" element={<ConferenceForm />} />
          <Route path="attendees/new" element={<AttendConferenceForm />} />
          <Route path="/presentations/new" element={<PresentationForm />} />
          <Route path="/attendees" element={<AttendeesList attendees={props.attendees} />} />
        </Routes>
      </div>
    </BrowserRouter >
  );
}

export default App;