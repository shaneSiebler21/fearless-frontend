window.addEventListener('DOMContentLoaded', async () => {


  // - populate our select box with locations
  const url = 'http://localhost:8000/api/locations/';

  const response = await fetch(url);

  if (response.ok) {
    const data = await response.json();
    // console.log(data);
    const selectTag = document.getElementById('location');
    for (let location of data.locations) {
      // console.log(data.states);
      const option = document.createElement('option');
      option.value = location.id;
      option.innerHTML = location.name;
      selectTag.appendChild(option);

      // Create an 'option' element
      // Set the '.value' property of the option element to the
      // state's abbreviation
      // Set the '.innerHTML' property of the option element to
      // the state's name
      // Append the option element as a child of the select tag
    }
  }

  // - handle the click button and submit the form

  const formTag = document.getElementById('create-conference-form');
  formTag.addEventListener('submit', async event => {
    event.preventDefault();
    const formData = new FormData(formTag);
    const obj = Object.fromEntries(formData);
    console.log(obj);
    const json = JSON.stringify(obj);
    // console.log(json);
    const conferenceURL = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        'Content-Type': 'application/json'
      },
    };
    const response = await fetch(conferenceURL, fetchConfig);
    if (response.ok) {
      formTag.reset();
      // const responseJson = await response.json();
      const newLocation = await response.json();
      // console.log(newLocation);
    }

  })
});
