window.addEventListener('DOMContentLoaded', async () => {
  const selectTag = document.getElementById('conference');

  const url = 'http://localhost:8000/api/conferences/';
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();

    for (let conference of data.conferences) {
      const option = document.createElement('option');
      option.value = conference.href;
      option.innerHTML = conference.name;
      selectTag.appendChild(option);
    }

    // Here, add the 'd-none' class to the loading icon
    let loadingIcon = document.getElementById('loading-conference-spinner');
    loadingIcon.classList.add('d-none');
    selectTag.classList.remove('d-none');
    // Here, remove the 'd-none' class from the select tag

  }
  const formTag = document.getElementById('create-attendee-form');
  formTag.addEventListener('submit', async event => {
    event.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData.entries()));
    // console.log(json);
    const attendeesURL = 'http://localhost:8001/api/attendees/';
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        'Content-Type': 'application/json'
      },
    };
    const response = await fetch(attendeesURL, fetchConfig);
    if (response.ok) {
      formTag.reset();

      const newAttendee = await response.json();
      const sucessMessage = document.getElementById('success-message');
      sucessMessage.classList.remove('d-none');
      // formTag.classlist.add('d-none');
      const removeBox = document.getElementById('create-attendee-form');
      const class3 = removeBox.classList;
      class3.add('d-none');
      selectTag.classList.remove('d-none');
    }

  })
});

