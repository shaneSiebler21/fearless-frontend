function createCard(name, description, pictureUrl, start, locationName) {
  return `
   <div class="col-sm-3">
    <div class="shadow-lg p-3 mb-5 bg-body rounded card">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer border-success">${start}</div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const start = details.conference.starts;
          const locationName = details.conference.location.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const html = createCard(title, description, pictureUrl, start, locationName);
          const column = document.querySelector('.row');
          column.innerHTML += html;
        }
      }

    }
  } catch (e) {
    // Figure out what to do if an error is raised - sent bad json 
    console.error(e)
  }

});

// window.addEventListener('DOMContentLoaded', async () => {

//   const url = 'http://localhost:8000/api/conferences/';
//   const response = await fetch(url);
//   console.log(response);

//   const data = await response.json();
//   console.log(data);

// });